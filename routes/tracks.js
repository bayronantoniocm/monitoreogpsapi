const { socket, connect, } = require('../connections/socket')
const { db, } = require('../connections/db')
const express = require('express');
const jwt = require('jsonwebtoken');
const request = require('request');
const router = express.Router();
const moment = require('moment');
const geolib = require('geolib');

// test url -> tracks/gettracks/?idversion=76&idequipo=56565,5656,6565
router.get('/gettracks/', (req, res, next) => {
    const { idversion, idequipo, idempresa } = req.query
    const filterDevise = idequipo ? idequipo.split(',') : []
    console.log('-->', req.query);
    if (socket) {
        socket.emit('getTracksWeb', idversion, (error, result) => {
            let response = []

            if (result) {
                console.log("1")
                if (result.length > 0) {
                    console.log("2")
                    result.forEach(movil => {
                        if (idempresa) {
                            if (movil.id_empresa == idempresa) {
                                if (filterDevise.length > 0) {
                                    filterDevise.forEach(item => {
                                        if (movil.id_equipo == item) {
                                            response.push(movil)
                                        }
                                    })
                                } else {
                                    response.push(movil)
                                }
                            }
                        } else {
                            console.log("idEmpresa null");
                            if (filterDevise.length > 0) {
                                filterDevise.forEach(item => {
                                    if (movil.id_equipo == item) {
                                        response.push(movil)
                                    }
                                })
                            } else {
                                response.push(movil)
                            }
                        }

                    })


                    response = response.map(item => {
                        const aux = {...item, }
                        const ubicacion = (aux.ubicacion || {})

                        aux.alerta = false

                        if (aux.bateria < 20) {
                            aux.bateria_baja = true
                            aux.alerta = true
                        }

                        if (aux.alerta_ubicacion === 'GPS_OFF') {
                            aux.ubicacion = false
                            aux.alerta = true
                        } else {
                            aux.ubicacion = true
                        }

                        //aux.ubicacion = (ubicacion.base64 || false)
                        //aux.bateria_baja = (aux.bateria_baja === 1)

                        // si la coneccion ex true, valida fecha movil
                        // si la fecha_movil + frecuencia (track.frecuencia) > fecha_actual now()
                        console.log('conectado:', aux.conectado, 'id_usuario:', aux.id_usuario)
                        aux.frecuencia = aux.frecuencia / 60000
                        return {...aux, }
                    })
                } else {
                    console.log("Sin resultado de Socket");
                    response = result
                }

                res.send(JSON.stringify(response))
            } else {
                res.send('Error procesando la solicitud')
            }
        })
    } else {
        res.send(new Error('Internal error'))
    }
})

// test url -> tracks/getlastubication/?idversion=76&idusuario=56565,5656,6565
router.get('/getlastubication/', (req, res, next) => {
    const { idversion, idusuario, } = req.query
    const filterUser = idusuario ? idusuario.split(',') : []

    if (socket) {
        socket.emit('getTracks', idversion, (error, result) => {
            let response = []

            if (result) {
                if (result.length > 0 && filterUser.length > 0) {
                    result.forEach(movil => {
                        filterUser.forEach(item => {
                            if (movil.id_usuario == item) response.push(movil)
                        })
                    })
                } else {
                    response = result
                }

                response = response.map(item => {
                    const aux = {...item, }
                    const ubicacion = (aux.ubicacion || {})
                    aux.alerta = false

                    if (aux.bateria < 20) {
                        aux.bateria_baja = true
                        aux.alerta = true
                    }

                    if (aux.alerta_ubicacion.length > 0) {
                        aux.ubicacion = (aux.alerta_ubicacion != 'GPS_OFF')
                        aux.alerta = true
                    }

                    //aux.ubicacion = (ubicacion.base64 || false)
                    //aux.bateria_baja = (aux.bateria_baja === 1)

                    // si la coneccion ex true, valida fecha movil
                    // si la fecha_movil + frecuencia (track.frecuencia) > fecha_actual now()

                    aux.frecuencia = aux.frecuencia / 60000
                    return {...aux, }
                })

                res.send(JSON.stringify(response))
            } else {
                res.send('Error procesando la solicitud')
            }
        })
    } else {
        res.send(new Error('Internal error'))
    }
})

// test url -> tracks/gettracks/?idversion=76&idequipo=56565,5656,6565
router.get('/gettrackstest/', (req, res, next) => {
    const formatedVars = {
        track: {
            version: '76',
            usuario: 21,
            fechaIni: '2018-09-04T14:31:47.322-0500',
            fechaFin: '2018-09-04T15:31:47.322-0500'
        }
    }

    return request({
        method: 'POST',
        url: 'https://szq5undw6k.execute-api.us-east-1.amazonaws.com/dev/gettracking',
        headers: {
            "Content-Type": 'application/json',
            Authorization: 'token',
        },
        body: formatedVars,
        json: true,
    }, (error, resp, body) => {
        if (!error) {
            const { track, } = formatedVars

            return db.query({
                    sql: `SELECT *
              FROM tracking.track
              WHERE id_usuario=?
                AND version_aplicacion=?
                AND fecha_movil BETWEEN ? AND ?
              ORDER by fecha_movil ASC`,
                    timeout: 40000, // 40s
                    values: [track.usuario, track.version, track.fechaIni, track.fechaFin, ]
                },
                (err, response) => {
                    res.send({ mysql: response.length, dynamo: body.length, })
                });
        } else {
            res.send(null)
        }
    })
})

// test url -> tracks/supervisefordevises/?idversion=76&iduser=56565
router.get('/supervisefordevises/', (req, res, next) => {
    const { idversion, iduser, id_publico_user, idempresa } = req.query
    if (idversion && iduser) {
        let appName = ''
        let port = 8011

        // refactor this code !!!
        if (idversion == 130) {
            appName = 'app0113'
        } else if (idversion == 142) {
            appName = 'app0090'
        } else if (idversion == 140) {
            appName = 'app0091'
        }
        // ---
        else if (idversion == 104) {
            appName = 'suite_1142_104'
            port = 8062
        } else if (idversion == 128) {
            appName = 'suite_1188_128'
            port = 8062
        } else if (idversion == 131) {
            appName = 'suite_1190_131'
            port = 8062
        } else if (idversion == 132) {
            appName = 'suite_1191_132'
            port = 8062
        } else if (idversion == 134) {
            appName = 'suite_1192_134'
            port = 8062
        } else if (idversion == 137) {
            appName = 'suite_1194_137'
            port = 8062
        } else if (idversion == 138) {
            appName = 'suite_1213_138'
            port = 8062
        } else if (idversion == 144) {
            appName = 'suite_1214_144'
            port = 8062
        } else if (idversion == 159) {
            appName = 'suite_1219_159'
            port = 8062
        } else if (idversion == 295) {
            appName = 'suite_1252_295'
            port = 8062
        } else if (idversion == 113) {
            appName = 'suite_1162_113'
            port = 8062
        } else if (idversion == 231) {
            appName = 'suite_1243_231'
            port = 8062
        } else if (idversion == 76) {
            appName = 'dinamic_silsa'
            port = 8062
        } else if (idempresa == 105 && idversion == 436) {
            appName = 'suite_105_436'
            port = 8062
        } else if (idempresa == 105 && idversion == 437) {
            appName = 'suite_105_436_'
            port = 8062
        } else if (idempresa == 1280 && idversion == 431) {
            appName = 'suite_1280_431'
            port = 8062
        }

        console.log(iduser, appName, port, id_publico_user);

        if (appName.length > 0) {
            var token = jwt.sign({
                "idUsuario": iduser,
                "idTenant": appName,
                "iat": 1526998185,
                "exp": 1758484097,
                "jti": "93ba76cb-8558-411a-85d2-0ac219384cf5"
            }, 'seraticseguro2018');
            console.log("token", token, " ", appName)

            request({
                url: `http://34.224.125.60:${port}/api_suite/equiposupervisor/`,
                headers: {
                    Authorization: token,
                }
            }, (error, response, body) => {
                if (response.statusCode !== 400) {
                    console.log('result ->', error, body)
                    if (body === "null") {
                        res.send([])
                    } else {
                        res.send(body)
                    }
                } else {
                    console.log('result ->', error, response)
                    res.send([])
                }
            })
        } else {
            //idpublico_user = "2764b74d3074221bda82cb240f27d6886cb1c4e33b05e9b71f1825b30ae7c64a"
            let filtros_a = []
            let filtro_o = {
                objeto: "usuario",
                campo: "id_publico",
                condicional: "=",
                parametro: "'" + id_publico_user + "'"
            }

            filtros_a.push(filtro_o)

            const filtros = {
                filtros: filtros_a
            }



            token = "db_" + idversion
            console.log('Consultando idVersion al GO ', token, " ", filtros)
            request({
                url: `https://ema2goreaderprepro.latinapps.co/apiread/funcion/campospaginacion/supervisorequipos/${idversion}/${id_publico_user}`,
                method: 'POST',
                headers: {
                    Authorization: token,
                },
                body: filtros,
                json: true,
            }, (error, response, body) => {
                if (response.statusCode !== 400) {

                    var newData = body.data
                        // const info = JSON.parse(body);
                    if (newData && newData.length > 0) {
                        //   console.log("data", info.equipos)

                        let equipos = []

                        for (var i = 0, len = newData[0].equipos.length; i < len; i++) {
                            let equipo_i = (newData[0].equipos[i]);
                            const eq = {
                                equipo: equipo_i
                            }

                            equipos.push(eq)
                        }
                        console.log("equipos", equipos)
                        res.send(equipos)
                    } else {
                        console.log("equipos []")
                        res.send([])
                    }

                } else {
                    console.log("equipos []")
                    res.send([])
                }
            })

        }
    } else {
        console.log('idVersion, or iduser invalid values');
        res.send([])
    }
})

router.post('/setTrack/', (req, res, next) => {

    console.log('setTrack -->', req.body);

    if (socket) {
        socket.emit('setTrack', req.body, (error, result) => {

        })
    }
    res.send(null)
})


router.post('/exportartrack/', (req, res, next) => {
    const { fechaIni, fechaFin, id_empresa, version, } = req.query

    console.log('-->', req.body);


    // reusult process
    let response = []

    return request({
        method: 'POST',
        url: 'https://szq5undw6k.execute-api.us-east-1.amazonaws.com/pro/exportar',
        headers: {
            "Content-Type": 'application/json',
            Authorization: 'token',
        },
        body: req.body,
        json: true,
    }, (error, response, body) => {
        if (!error) {
            const result = []
            body.forEach((track, index) => {
                result.push(track)
            })

            res.send(result)
        } else {
            res.send(null)
        }
    })
})

// // test url -> http://35.173.1.242:3000/tracks/gettraking/?idversion=76&idusuario=21&fechaini=2018-09-03 00:00&fechafin=2018-09-03 23:00
// router.get('/gettraking/', (req, res, next) => {
//     let { idversion, idusuario, fechaini, fechafin, idispositivo,parada,distancia_parada } = req.query

//     console.log('-->', req.query);
//     console.log('-->', idversion);
//     console.log('-->', idusuario);
//     console.log('-->', fechaini);
//     console.log('-->', idispositivo);

//     // reusult process
//     let response = []
//     if (idispositivo) {
//         idusuario = idispositivo
//     }
//     if(!parada){
//       parada = false
//     }
//     if(!distancia_parada){
//     distancia_parada = 50
//     }
//     const formatedVars = {
//         track: {
//             version: idversion,
//             usuario: Number.parseInt(idusuario),
//             fechaIni: fechaini,
//             fechaFin: fechafin
//         }
//     }

//     return request({
//         method: 'POST',
//         url: 'https://szq5undw6k.execute-api.us-east-1.amazonaws.com/dev/gettracking',
//         headers: {
//             "Content-Type": 'application/json',
//             Authorization: 'token',
//         },
//         body: formatedVars,
//         json: true,
//     }, (error, response, body) => {
//         if (!error) {
//             const result = []
//             console.log(body.length)
//             let ubicacion_ref
//             let parada_ref
//             let index_track = 0
//             body.forEach((item, index) => {
//                 const track = {...item, }

//                 // temporal_code
//                 track.fecha_captura_gps = moment.tz(track.fecha_captura_gps, 'America/Bogota').format();
//                 track.fecha_movil = moment.tz(track.fecha_movil, 'America/Bogota').format();
//                 track.fecha_proximo_seguimiento = moment.tz(track.fecha_proximo_seguimiento, 'America/Bogota').format();

//                 if (track.fecha_servidor) {
//                     track.fecha_servidor = moment.tz(track.fecha_servidor, 'America/Bogota').format();
//                 }

//                 if (index === 0) {
//                     track.tipo_track = 100
//                     ubicacion_ref = track
//                     parada_ref = track
//                     track
//                 } else if (index === (body.length - 1)) {
//                     console.log(index)
//                     track.tipo_track = 101
//                 } else if (track.tipo_track === 2) {
//                     if (track.alerta_ubicacion === 'GPS_OFF') track.tipo_track = 102
//                     else if (track.alerta_ubicacion === 'BATERIA_BAJA') track.tipo_track = 103
//                 }

//                 track.conexion = (track.conexion === 1)
//                 if (track.tipo_track === 2) {
//                     try {
//                         let distancia_ubicacion_ref = geolib.getPreciseDistance({ latitude: track.latitud_posicion, longitude: track.longitud_posicion }, { latitude: ubicacion_ref.latitud_posicion, longitude: ubicacion_ref.longitud_posicion })
//                             // let distancia_parada_ref = geolib.getPreciseDistance({ latitude: track.latitud_posicion, longitude: track.longitud_posicion }, { latitude: parada_ref.latitud_posicion, longitude: parada_ref.longitud_posicion })
//                         let distanciamax = track.presicion
//                         if (distanciamax < distancia_parada) {
//                             distanciamax = distancia_parada // en esta caso seria 50
//                         } else {
//                             distanciamax = distanciamax + ubicacion_ref.presicion
//                                 // if(distanciamax)
//                         }
//                         console.log("distanciamax ", distanciamax, " ur: " + distancia_ubicacion_ref)
//                         if (distancia_ubicacion_ref > distanciamax) {
//                             result.push(track)
//                             ubicacion_ref = track
//                             if (ubicacion_ref === parada_ref) {
//                                 console.log("distancia1 ", distancia_ubicacion_ref, " nueva Ubicacion de Ref Agrega ", index_track, " " + track.presicion)
//                             } else {
//                                 console.log("distancia2 ", distancia_ubicacion_ref, " Agrega asigna ubicacion_ref = parada_ref ", index_track, " " + track.presicion)
//                                 parada_ref = ubicacion_ref
//                             }
//                             index_track = index_track + 1
//                         } else {
//                             if (index === 0 || index === (body.length - 1)) {
//                                 console.log("distancia3 ", distancia_ubicacion_ref, "Agrega ", index_track)
//                                 if (index === (body.length - 1)) ubicacion_ref.tipo_track = 101 //result.push(ubicacion_ref)
//                                 else result.push(track)
//                                 index_track = index_track + 1
//                             } else {
//                                 // if (distancia_parada_ref > distancia_parada) {
//                                 //   console.log("distancia4 ", distancia_parada_ref, "-", distancia_ubicacion_ref, "Agrega")
//                                 //  result.push(track)
//                                 //} else {

//                                 if (ubicacion_ref != parada_ref) {
//                                     parada_ref = ubicacion_ref
//                                         //   console.log("distancia5 ", distancia_parada_ref, " nueva Parada de Ref no Agrega ")
//                                 } else {
//                                     // console.log("distancia6 ", distancia_parada_ref, " no Agrega ")
//                                 }
//                                 //}

//                             }
//                         }

//                     } catch (e) {
//                         console.log(e);
//                     }
//                 } else {
//                     result.push(track)
//                 }
//                 /*
//                 if (track.id_track_parada === 0 || index === 0 || index === (body.length -1)) {
//                   if (track.tipo_track === 2 ) {
//                     if (track.tiempo_parada === 0) result.push(track)
//                   } else {

//                   }
//                 }*/
//             })

//             res.send(result)
//         } else {
//             res.send(null)
//         }
//     })
// })

// function tracking_parada(){

// }

// router.get('/gettracking/', (req, res, next) => {
//     const { idversion, idusuario, fechaini, fechafin, } = req.query

//     console.log('-->', req.query);
//     console.log('-->', idversion);
//     console.log('-->', idusuario);
//     console.log('-->', fechaini);
//     const distancia_parada = 50

//     // reusult process
//     let response = []
//     const formatedVars = {
//         track: {
//             version: idversion,
//             usuario: Number.parseInt(idusuario),
//             fechaIni: fechaini,
//             fechaFin: fechafin
//         }
//     }

//     return request({
//         method: 'POST',
//         url: 'https://szq5undw6k.execute-api.us-east-1.amazonaws.com/dev/gettracking',
//         headers: {
//             "Content-Type": 'application/json',
//             Authorization: 'token',
//         },
//         body: formatedVars,
//         json: true,
//     }, (error, response, body) => {
//         if (!error) {
//             const result = []
//             console.log(body.length)
//             let ubicacion_ref
//             let parada_ref
//             let index_track = 0
//             body.forEach((item, index) => {
//                 const track = {...item, }

//                 // temporal_code
//                 track.fecha_captura_gps = moment.tz(track.fecha_captura_gps, 'America/Bogota').format();
//                 track.fecha_movil = moment.tz(track.fecha_movil, 'America/Bogota').format();
//                 track.fecha_proximo_seguimiento = moment.tz(track.fecha_proximo_seguimiento, 'America/Bogota').format();

//                 if (track.fecha_servidor) {
//                     track.fecha_servidor = moment.tz(track.fecha_servidor, 'America/Bogota').format();
//                 }

//                 if (index === 0) {
//                     track.tipo_track = 100
//                     ubicacion_ref = track
//                     parada_ref = track
//                     track
//                 } else if (index === (body.length - 1)) {
//                     console.log(index)
//                     track.tipo_track = 101
//                 } else if (track.tipo_track === 2) {
//                     if (track.alerta_ubicacion === 'GPS_OFF') track.tipo_track = 102
//                     else if (track.alerta_ubicacion === 'BATERIA_BAJA') track.tipo_track = 103
//                 }

//                 track.conexion = (track.conexion === 1)
//                 if (track.tipo_track === 2) {
//                     try {
//                         let distancia_ubicacion_ref = geolib.getPreciseDistance({ latitude: track.latitud_posicion, longitude: track.longitud_posicion }, { latitude: ubicacion_ref.latitud_posicion, longitude: ubicacion_ref.longitud_posicion })
//                             // let distancia_parada_ref = geolib.getPreciseDistance({ latitude: track.latitud_posicion, longitude: track.longitud_posicion }, { latitude: parada_ref.latitud_posicion, longitude: parada_ref.longitud_posicion })
//                         let distanciamax = track.presicion
//                         if (distanciamax < distancia_parada) {
//                             distanciamax = distancia_parada // en esta caso seria 50
//                         } else {
//                             distanciamax = distanciamax + ubicacion_ref.presicion
//                                 // if(distanciamax)
//                         }
//                         console.log("distanciamax ", distanciamax, " ur: " + distancia_ubicacion_ref)
//                         if (distancia_ubicacion_ref > distanciamax) {
//                             result.push(track)

//                             ubicacion_ref = track
//                             if (ubicacion_ref === parada_ref) {
//                                 console.log("distancia1 ", distancia_ubicacion_ref, " nueva Ubicacion de Ref Agrega ", index_track, " " + track.presicion)

//                             } else {
//                                 console.log("distancia2 ", distancia_ubicacion_ref, " Agrega asigna ubicacion_ref = parada_ref ", index_track, " " + track.presicion)

//                                 parada_ref = ubicacion_ref
//                             }
//                             index_track = index_track + 1
//                         } else {
//                             if (index === 0 || index === (body.length - 1)) {
//                                 console.log("distancia3 ", distancia_ubicacion_ref, "Agrega ", index_track)
//                                 if (index === (body.length - 1)) ubicacion_ref.tipo_track = 101 //result.push(ubicacion_ref)
//                                 else result.push(track)
//                                 index_track = index_track + 1
//                             } else {
//                                 // if (distancia_parada_ref > distancia_parada) {
//                                 //   console.log("distancia4 ", distancia_parada_ref, "-", distancia_ubicacion_ref, "Agrega")
//                                 //  result.push(track)
//                                 //} else {

//                                 if (ubicacion_ref != parada_ref) {
//                                     parada_ref = ubicacion_ref
//                                         //   console.log("distancia5 ", distancia_parada_ref, " nueva Parada de Ref no Agrega ")
//                                 } else {
//                                     // console.log("distancia6 ", distancia_parada_ref, " no Agrega ")
//                                 }
//                                 //}

//                             }
//                         }

//                     } catch (e) {
//                         console.log(e);
//                     }
//                 } else {
//                     result.push(track)
//                 }
//                 /*
//                 if (track.id_track_parada === 0 || index === 0 || index === (body.length -1)) {
//                   if (track.tipo_track === 2 ) {
//                     if (track.tiempo_parada === 0) result.push(track)
//                   } else {

//                   }
//                 }*/
//             })

//             res.send(result)
//         } else {
//             res.send(null)
//         }
//     })
// })

// // detalle_movil, id_track, imei ordenado por fecha movil desc, cojo el primero, y ese lo actualizo el detalle movil


const getTracking = (idversion, idusuario, fechaini, fechafin, distancia_parada, parada) => {
    return new Promise(async function(resolve, reject) {
        const result = []


        console.log('-->', idversion);
        console.log('-->', idusuario);
        console.log('-->', fechaini);
        console.log('-->', fechafin);
        console.log('-->', distancia_parada);
        console.log('-->', parada);



        const formatedVars = {
            track: {
                version: idversion,
                usuario: Number.parseInt(idusuario),
                fechaIni: fechaini,
                fechaFin: fechafin
            }
        }

        return request({
            method: 'POST',
            url: 'https://szq5undw6k.execute-api.us-east-1.amazonaws.com/dev/gettracking',
            headers: {
                "Content-Type": 'application/json',
                Authorization: 'token',
            },
            body: formatedVars,
            json: true,
        }, (error, response, body) => {
            if (!error) {

                console.log(body.length)
                let ubicacion_ref

                let index_track = 0
                let inicio_parada


                body.forEach((item, index) => {
                    const track = {...item, }

                    // temporal_code
                    track.fecha_captura_gps = moment.tz(track.fecha_captura_gps, 'America/Bogota').format();
                    track.fecha_movil = moment.tz(track.fecha_movil, 'America/Bogota').format();
                    track.fecha_proximo_seguimiento = moment.tz(track.fecha_proximo_seguimiento, 'America/Bogota').format();

                    if (track.fecha_servidor) {
                        track.fecha_servidor = moment.tz(track.fecha_servidor, 'America/Bogota').format();
                    }

                    if (index === 0) {

                        ubicacion_ref = track

                        //  
                    }
                    //
                    else if (track.tipo_track === 2) {
                        if (track.alerta_ubicacion === 'GPS_OFF') track.tipo_track = 102
                        else if (track.alerta_ubicacion === 'BATERIA_BAJA') track.tipo_track = 103
                    }

                    track.conexion = (track.conexion === 1)
                    if (track.tipo_track === 2) {
                        if (parada) {
                            try {
                                let distancia_ubicacion_ref = geolib.getPreciseDistance({ latitude: track.latitud_posicion, longitude: track.longitud_posicion }, { latitude: ubicacion_ref.latitud_posicion, longitude: ubicacion_ref.longitud_posicion })
                                    // let distancia_parada_ref = geolib.getPreciseDistance({ latitude: track.latitud_posicion, longitude: track.longitud_posicion }, { latitude: parada_ref.latitud_posicion, longitude: parada_ref.longitud_posicion })
                                let distanciamax = track.presicion
                                if (distanciamax < distancia_parada) { //evaluamos si la distancia de al precion es menor a la distancia de la paradas
                                    distanciamax = distancia_parada // en esta caso seria 50
                                } else {
                                    distanciamax = distanciamax + ubicacion_ref.presicion // es cuando la precision es muy grande
                                        // if(distanciamax)
                                }
                                console.log("distanciamax ", distanciamax, " dub_ref: " + distancia_ubicacion_ref)
                                if (distancia_ubicacion_ref > distanciamax) {
                                    console.log("No es parada ", index_track)
                                        //no es parada
                                    result.push(track)

                                    if (ubicacion_ref) {
                                        console.log("Ubicacion Referencia..a", ubicacion_ref.parada)
                                        const end = moment(track.fecha_movil)
                                        const init = moment(ubicacion_ref.fecha_movil)
                                        const duration = moment.duration(end.diff(init))
                                        if (ubicacion_ref.parada == true) {
                                            Object.assign(ubicacion_ref, { duracion_parada: moment(duration).format('HH:mm') })
                                            console.log("duracion parada ..a", duration.asMilliseconds())
                                        }
                                        ubicacion_ref = track
                                    }
                                    index_track = index_track + 1

                                } else {
                                    console.log("posible parada", index, " - ", index_track)
                                        //es una posible parada
                                    if (index === 0 || index === (body.length - 1)) {
                                        if (index === 0) {
                                            ubicacion_ref.tipo_track = 100
                                                //no es paradad porque porque es el primero o ultimo
                                            console.log("primera ubicacion adicionada", index, " - ", index_track)
                                            result.push(track)
                                        } else if (index === (body.length - 1)) {
                                            console.log("ultima ubicacion", index, " - ", index_track)
                                                //if (ubicacion_ref) {
                                            track.tipo_track = 101 //result.push(ubicacion_ref)
                                            const end = moment(track.fecha_movil)
                                            const init = moment(ubicacion_ref.fecha_movil)
                                            const duration = moment.duration(end.diff(init))
                                            Object.assign(track, { duracion_parada: moment(duration).format('HH:mm') })
                                            Object.assign(track, { parada: true })
                                            console.log("duracion parada ultima ubicacion", duration.asMilliseconds())
                                            result.push(track)

                                        }
                                        index_track = index_track + 1
                                    } else {
                                        console.log("es una parada", index, " - ", index_track, " up", ubicacion_ref.parada)


                                        Object.assign(ubicacion_ref, { parada: true })

                                        console.log("asignando parada", ubicacion_ref.parada)

                                    }
                                }
                            } catch (e) {
                                console.log(e);
                            }
                        } else {
                            result.push(track)
                        }


                    } else {
                        result.push(track)
                    }

                })
                resolve(result)
            }
        })

    })

}
router.get('/gettracking/', (req, res, next) => {
    const { idversion, idusuario, fechaini, fechafin, } = req.query
    const distancia_parada = 50
    res.send(getTracking(idversion, idusuario, fechaini, fechafin, 0, false))

})

// test url -> http://35.173.1.242:3000/tracks/gettraking/?idversion=76&idusuario=21&fechaini=2018-09-03 00:00&fechafin=2018-09-03 23:00
router.get('/gettraking/', async(req, res, next) => {

    let { idversion, idusuario, fechaini, fechafin, idispositivo, parada, distancia_parada } = req.query

    console.log("getTrack parada 1", parada, distancia_parada)
    if (parada) {
        console.log("getTrack", parada, distancia_parada)
        if (parada) {

        }
    } else {
        console.log("getTrack parada2", parada, distancia_parada)
        parada = true
        distancia_parada = 50
    }
    let response = await getTracking(idversion, idispositivo, fechaini, fechafin, distancia_parada, parada)
    res.send(response)

})



module.exports = router
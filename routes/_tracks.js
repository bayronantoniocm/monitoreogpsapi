const { socket, connect, } = require('../connections/socket')
const { db, } = require('../connections/db')
const express = require('express');
const jwt = require('jsonwebtoken');
const request = require('request');
const router = express.Router();
const moment = require('moment');

// test url -> tracks/gettracks/?idversion=76&idequipo=56565,5656,6565
router.get('/gettracks/', (req, res, next) => {
  const { idversion, idequipo, } = req.query
  const filterDevise = idequipo ? idequipo.split(',') : []

  if (socket) {
    socket.emit('getTracksWeb', idversion, (error, result) => {
      let response = []

      if (result) {
        if (result.length > 0 && filterDevise.length > 0) {
          result.forEach(movil => {
            filterDevise.forEach(item => {
              if (movil.id_equipo == item) response.push(movil)
            })
          })
        } else {
          response = result
        }

        response = response.map(item => {
          const aux = { ...item, }
          const ubicacion = (aux.ubicacion || {})

          aux.bateria_baja = (aux.bateria < 20)
          aux.ubicacion = (aux.alerta_ubicacion !== 'GPS_OFF')

          //aux.ubicacion = (ubicacion.base64 || false)
          //aux.bateria_baja = (aux.bateria_baja === 1)

          // si la coneccion ex true, valida fecha movil
          // si la fecha_movil + frecuencia (track.frecuencia) > fecha_actual now()

          aux.frecuencia = aux.frecuencia / 60000
          return { ...aux, }
        })

        res.send(JSON.stringify(response))
      } else {
        res.send('Error procesando la solicitud')
      }
    })
  } else {
    res.send(new Error('Internal error'))
  }
})

// test url -> tracks/gettracks/?idversion=76&idequipo=56565,5656,6565
router.get('/gettrackstest/', (req, res, next) => {
  const formatedVars = {
    track: {
      version: '76',
      usuario: 21,
      fechaIni: '2018-09-04T14:31:47.322-0500',
      fechaFin: '2018-09-04T15:31:47.322-0500'
    }
  }

  return request({
    method: 'POST',
    url: 'https://szq5undw6k.execute-api.us-east-1.amazonaws.com/dev/gettracking',
    headers: {
      "Content-Type": 'application/json',
      Authorization: 'token',
    },
    body: formatedVars,
    json: true,
  }, (error, resp, body) => {
    if (!error) {
      const { track, } = formatedVars

      return db.query({
        sql: `SELECT *
              FROM tracking.track
              WHERE id_usuario=?
                AND version_aplicacion=?
                AND fecha_movil BETWEEN ? AND ?
              ORDER by fecha_movil ASC`,
        timeout: 40000, // 40s
        values: [ track.usuario, track.version, track.fechaIni, track.fechaFin, ]
      },
      (err, response) => {
        res.send({ mysql: response.length, dynamo: body.length, })
      });
    } else {
      res.send(null)
    }
  })
})

// test url -> tracks/supervisefordevises/?idversion=76&iduser=56565
router.get('/supervisefordevises/', (req, res, next) => {
  const { idversion, iduser, } = req.query
  if (idversion && iduser) {
    let appName = ''
    let port = 8011

    /*
      Primax 104,
      Vertical 128
      Selvapits 131,
      Digepul 132,
      Lavisa 134,
      Kanazawa 137,
      SanIsidro 138,
      Solar 144,
      Sercorisac 159,
    */

    // refactor this code !!!
    if (idversion == 130) {
      appName = 'app0113'
    } else if (idversion == 142) {
      appName = 'app0090'
    } else if (idversion == 140) {
      appName = 'app0091'
    } else if (idversion == 124) {
      appName = 'app0110'
    }
    // ---
    else if (idversion == 104) {
      appName = 'suite_1142_104'
      port = 8062
    } else if (idversion == 128) {
      appName = 'suite_1188_128'
      port = 8062
    } else if (idversion == 131) {
      appName = 'suite_1190_131'
      port = 8062
    } else if (idversion == 132) {
      appName = 'suite_1191_132'
      port = 8062
    } else if (idversion == 134) {
      appName = 'suite_1192_134'
      port = 8062
    } else if (idversion == 137) {
      appName = 'suite_1194_137'
      port = 8062
    } else if (idversion == 138) {
      appName = 'suite_1213_138'
      port = 8062
    } else if (idversion == 144) {
      appName = 'suite_1214_144'
      port = 8062
    } else if (idversion == 159) {
      appName = 'suite_1219_159'
      port = 8062
    }
    // ---
    else if (idversion == 76) {
      appName = 'dinamic_silsa'
      port = 8011
    }
    /*else if (idversion == 76) {
      appName = 'suite_267_12'
      port = 8062
    }*/

    console.log(iduser, appName, port);

    if (appName.length > 0) {
     var token = jwt.sign({
         "idUsuario": iduser,
         "idTenant": appName,
         "iat": 1526998185,
         "exp": 1558484097,
         "jti": "93ba76cb-8558-411a-85d2-0ac219384cf5"
     }, 'seraticseguro2018');

     // console.log(token);

     request({
       url: `http://34.224.125.60:${port}/api_suite/equiposupervisor/`,
       headers: {
         Authorization: token,
       }
     }, (error, response, body) => {
       if (!error) {
         res.send(body)
       } else {
         console.log(error)
         res.send([])
       }
     })
   } else {
    res.send([]) 
   }
  } else {
    res.send([])
  }
})

// test url -> tracks/gettracking/?idversion=76&idusuario=56565&fechaini=''&fechafin=''
router.get('/gettracking/', (req, res, next) => {
  const { idversion, idusuario, fechaini, fechafin, } = req.query

  console.log('-->', req.query);
  console.log('-->', idversion);
  console.log('-->', idusuario);
  console.log('-->', fechaini);
  console.log('-->', fechafin);

  // reusult process
  let response = []
  const formatedVars = {
    track: {
      version: idversion,
      usuario: Number.parseInt(idusuario),
      fechaIni: fechaini,
      fechaFin: fechafin
    }
  }

  return request({
    method: 'POST',
    url: 'https://szq5undw6k.execute-api.us-east-1.amazonaws.com/dev/gettracking',
    headers: {
      "Content-Type": 'application/json',
      Authorization: 'token',
    },
    body: formatedVars,
    json: true,
  }, (error, response, body) => {
    if (!error) {
      const result = []
      console.log('response tracking -->', body.length)
      body.forEach((item, index) => {
        const track = { ...item, }

        // temporal_code
        track.fecha_captura_gps = moment.tz(track.fecha_captura_gps, 'America/Bogota').format();
        track.fecha_movil = moment.tz(track.fecha_movil, 'America/Bogota').format();
        track.fecha_proximo_seguimiento = moment.tz(track.fecha_proximo_seguimiento, 'America/Bogota').format();

        if (track.fecha_servidor) {
          track.fecha_servidor = moment.tz(track.fecha_servidor, 'America/Bogota').format();
        }
        //

        if (track.id_track_parada === 0 || index === 0 || index === (body.length -1)) {
          if (track.tipo_track === 2 ) {
            if (track.tiempo_parada === 0) result.push(track)
          } else {
            result.push(track)
          }
        }
      })

      res.send(result)
    } else {
      res.send(null)
    }
  })
})

// test url -> tracks/gettracking/?idversion=76&idusuario=56565&fechaini=''&fechafin=''
router.get('/resumetracking/', (req, res, next) => {
  const { idversion, idusuario, fechaini, fechafin, } = req.query

  console.log('-->', req.query);
  console.log('-->', idversion);
  console.log('-->', idusuario);
  console.log('-->', fechaini);
  console.log('-->', fechafin);

  // reusult process
  let response = []
  const formatedVars = {
    track: {
      version: idversion,
      usuario: Number.parseInt(idusuario),
      fechaIni: fechaini,
      fechaFin: fechafin
    }
  }

  return request({
    method: 'POST',
    url: 'https://szq5undw6k.execute-api.us-east-1.amazonaws.com/dev/gettracking',
    headers: {
      "Content-Type": 'application/json',
      Authorization: 'token',
    },
    body: formatedVars,
    json: true,
  }, (error, response, body) => {
    if (!error) {
      const result = []
      body.forEach((track, index) => {
        // const { track, } = item
        // console.log('track -->', track)
  	    // console.log('flag result ->', track.id_track === 0 || index === 0 || index === (body.length - 1))
  	    // if (track.id_track_parada === 0 || index === 0 || index === (body.length -1)) {
        //   result.push(track)
        // }
      })

      res.send(result)
    } else {
      res.send(null)
    }
  })
})

// test url -> http://35.173.1.242:3000/tracks/gettraking/?idversion=76&idusuario=21&fechaini=2018-09-03 00:00&fechafin=2018-09-03 23:00
router.get('/gettraking/', (req, res, next) => {
  const { idversion, idusuario, fechaini, fechafin, } = req.query

  console.log('-->', req.query);
  console.log('-->', idversion);
  console.log('-->', idusuario);
  console.log('-->', fechaini);
  console.log('-->', fechafin);

  // reusult process
  let response = []
  const formatedVars = {
    track: {
      version: idversion,
      usuario: Number.parseInt(idusuario),
      fechaIni: fechaini, // '2018-09-03 00:00'
      fechaFin: fechafin // '2018-09-03 23:00'
    }
  }


  // testing dynamodb calls
  const initialTime = new Date()

  request({
    method: 'POST',
    url: 'https://szq5undw6k.execute-api.us-east-1.amazonaws.com/dev/gettracking',
    headers: {
      "Content-Type": 'application/json',
      Authorization: 'token',
    },
    body: formatedVars,
    json: true,
  }, (error, response, body) => {
      const endTime = new Date()
      console.log('Dynamo request time ->',
        moment.tz(initialTime, 'America/Bogota').format(), ' - ',
        moment.tz(endTime, 'America/Bogota').format())
  })
  // ---

  //
  return db.query({
    sql: 'SELECT * FROM tracking.track WHERE id_usuario=? AND version_aplicacion=? and fecha_movil BETWEEN ? AND ? ORDER by fecha_movil ASC',
    timeout: 40000, // 40s
    values: [ idusuario, idversion, fechaini, fechafin, ]
  }, (err, response) => {
    const result = []
    if (!err) {
      console.log('response tracking -->', response.length)
      response.forEach((track, index) => {
        // temporal_code
        track.fecha_captura_gps = moment.tz(track.fecha_captura_gps, 'America/Bogota').format();
        track.fecha_movil = moment.tz(track.fecha_movil, 'America/Bogota').format();
        track.fecha_proximo_seguimiento = moment.tz(track.fecha_proximo_seguimiento, 'America/Bogota').format();
        track.bateria_baja = (track.bateria < 20)

        if (track.fecha_servidor) {
          track.fecha_servidor = moment.tz(track.fecha_servidor, 'America/Bogota').format();
        }
        //

        if (track.id_track_parada === 0 || index === 0 || index === (response.length -1)||track.tipo_track ===1 )
        {
          if (index === 0) track.tipo_track = 100
          else if (index === (response.length - 1)) track.tipo_track = 101
          else if (track.tipo_track === 2) {
            if (track.alerta_ubicacion === 'GPS_OFF') track.tipo_track = 102
            else if (track.alerta_ubicacion === 'BATERIA_BAJA') track.tipo_track = 103
          }

          track.conexion = (track.conexion === 1)
          result.push(track)
        }
      })

      res.send(result)
    } else {
      console.log(err)
      res.send('Internal error')
    }
  });
})

// detalle_movil, id_track, imei ordenado por fecha movil desc, cojo el primero, y ese lo actualizo el detalle movil
// actualizar detalle movil

  // test url -> http://35.173.1.242:3000/tracks/detailupdate/?idtrack=559&imeimovil=355030094919386&detallemovil=tetetete
  router.get('/detailupdate/', (req, res, next) => {
    const { idtrack, imeimovil, detallemovil, } = req.query

    console.log('-->', idtrack);
    console.log('-->', imeimovil);
    console.log('-->', detallemovil);

    //
    return db.query({
      sql: `
      update track
      	set detalle_movil = ?
          where id = (
      		select t.id
      		from (
      			select id
      			from track
      			where id_track = ? and imei_movil=?
      			order by fecha_movil desc
      			limit 1
      		) as t
      	)
      `,
      timeout: 40000, // 40s
      values: [ detallemovil, idtrack, imeimovil, ]
    }, (err, response) => {
      const result = []
      if (err) {
        console.log(err)
        res.send('Internal error')
      } else {
        res.send({resultAction: true})
      }
    });
  })



module.exports = router

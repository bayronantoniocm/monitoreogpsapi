#!/bin/bash

echo Script para actualizar el proyecto

echo Descargamos las actualizaciones del proyecto

sleep 1s

#git pull origin develop

sleep 1s

echo Deteniendo servicio SERATIC

forever stop "apitrack_tds"   

echo Borrando Logs

rm /home/bitnami/.forever/apitrack_tds.log

echo Iniciando Servicio

forever start -a --uid "apitrack_tds" bin/www

sleep 1s

echo Cargando el log

tail -f /home/bitnami/.forever/apitrack_tds.log

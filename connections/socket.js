const socketCluster = require('socketcluster-client')
const schedule = require('node-schedule');
const config = require('./config');

let connect = false
let socket  = null

const { socket: { url, port, path, hostname, }, } = config
const options = { port, path, hostname, autoConnect: true, autoReconnect: true, };

// const reconect = schedule.scheduleJob('1 * * * * *', function(){
//   if (!connect) {
//     try {
//
//     } catch (error) {
//       console.log('Error connecting with service');
//     }
//   }
// });

try {
  socket = socketCluster.create(options);
  socket.on('connect', () => { connect = true })
  socket.on('error', () => { connect = false })

  console.log('CONNECTED');
} catch (error) {
  console.log('Error connecting with service');
}

module.exports = {
  socket,
  connect,
}
